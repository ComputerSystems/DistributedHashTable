package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"net/http"
	"hash/fnv"
	"io/ioutil"
	"time"
	"encoding/base64"
	"bytes"
	"flag"
	"strings"
)

type test_struct struct {
    Test string
}

type KeyValue struct{
	Encoding string `json:"encoding"`
	Data string	`json:"data"`
}

type Entry struct{
	Key KeyValue `json:"key"`
	Value KeyValue `json:"value"`
}

type QueryReturn struct{
	Key KeyValue`json:"key"`
	Value bool	`json:"value"`
}

type SetReturn struct{
	Keys_added int `json:"keys_added"`
	Keys_failed []Entry `json:"keys_failed"`
}

//Hash - EndPoint mapping
var servers = make(map[int]string)
var numServers = 0
var myPort = ":8080"
var DBG = 0

func fnvHash(s string) uint32 {
	h := fnv.New32a()
	h.Write([]byte(s))
	return h.Sum32()
}

func sendSetResponse(rw http.ResponseWriter, sResp map[int]*http.Response){
	//combine and send the output.
	var setRet SetReturn
	isSuccess := true
	for _, response := range sResp {
		defer response.Body.Close()
		body, _ := ioutil.ReadAll(response.Body)
		if DBG != 0 {
			fmt.Println("response Body:", string(body))	
		}
	    if response.StatusCode != 200 {
			isSuccess = false			
		}			
	    var sr SetReturn
	    err := json.Unmarshal(body, &sr)
	    if err != nil {
			log.Fatal(err)
			return
		} 
	    setRet.Keys_added += sr.Keys_added
	    setRet.Keys_failed = append(setRet.Keys_failed, sr.Keys_failed...)		
    }
	if DBG != 0 {
	    fmt.Println("Sending Return To Client", setRet)
    }
    if len(setRet.Keys_failed) != 0 && isSuccess {
    	//send failure
    	rw.WriteHeader(http.StatusNotFound)    	
    } else {
    	//send Success
    	rw.WriteHeader(http.StatusOK)
    }
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(setRet)
	if DBG != 0 {
		fmt.Println("Sent return Size:", len(setRet.Keys_failed))
	}
}

func setHandler(c chan *http.Response, Entries []Entry, sID int){
	client := &http.Client{}
	endPoint := servers[sID] + "set"
	jsonValue, _ := json.Marshal(Entries)
	if DBG != 0 {
		fmt.Println(string(jsonValue))
	}
	request, err := http.NewRequest("PUT", endPoint, bytes.NewBuffer(jsonValue))
	if err != nil {
		panic(err)
		return
	}
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
		return
	} else {
		c <- response
	}
}

func unknownMethodHandler(rw http.ResponseWriter, req *http.Request){
	fmt.Println("Unknown Method:", req.Method)
	rw.WriteHeader(http.StatusMethodNotAllowed)    		
}

func setReqHandler(rw http.ResponseWriter, req *http.Request) {
	fmt.Println("Serving Set Request")
	if req.Method != "PUT" {
		unknownMethodHandler(rw, req)
		return
	}
    defer req.Body.Close()    
    body, _ := ioutil.ReadAll(req.Body)
    if DBG != 0 {
		fmt.Println("req Body:", string(body))	    
	}
	entry := make([]Entry,0)
	err := json.Unmarshal(body, &entry)
    if err != nil {
        panic(err)
        return
    }
    serverEntries := make(map[int][]Entry, numServers)
	serverResp := make(map[int]*http.Response, numServers)
	chans := make([]chan *http.Response, numServers)
	for i := 0; i < numServers; i++ {
		chans[i] = make(chan *http.Response)		
    }
    
    for _, keyvalue := range entry {
	    if DBG != 0 {
    		fmt.Println(keyvalue.Key.Data, " - ", keyvalue.Value.Data)
		}
    	Key := keyvalue.Key
    	dKey := Key.Data
    	if Key.Encoding == "binary" {
			decoded, err := base64.StdEncoding.DecodeString(Key.Data)
			if err != nil {
				fmt.Println("decode error:", err)
				return
			}
			dKey = string(decoded)
		} 
    	serverID := int(fnvHash(dKey) % uint32(numServers))
    	serverEntries[serverID] = append(serverEntries[serverID], keyvalue)
    }
    
    for sID, Entries := range serverEntries {
    	go setHandler(chans[sID], Entries, sID)	
    }
    
    for sID,_ := range serverEntries {
    	select {
			case resp := <- chans[sID]:
				if DBG != 0 {
					fmt.Println("Got Response from Server:", sID)
				}
				serverResp[sID] = resp
			case <-time.After(time.Second * 1):
				fmt.Println("timeout on chan", sID)
    	}
    }
    if DBG != 0 {
	    fmt.Println("Got responses from all Servers")
    }
    sendSetResponse(rw, serverResp)
}

func sendFetchResponse(rw http.ResponseWriter, sResp map[int]*http.Response){
	//combine and send the output.
	fetchRet := make([]Entry, 0)
	isSuccess := true
	for _, response := range sResp {
		defer response.Body.Close()
		body, _ := ioutil.ReadAll(response.Body)
		if DBG != 0 {
			fmt.Println("response Body:", string(body))	
		}
	    if response.StatusCode != 200 {
			isSuccess = false			
		}
	    entry := make([]Entry, 0)
		err := json.Unmarshal(body, &entry)
	    if err != nil {
			log.Fatal(err)
			return
		} 
		fetchRet = append(fetchRet, entry...)
    }
    if DBG != 0 {
	    fmt.Println("Sending Return To Client", fetchRet)
    }
    if isSuccess {
    	//send Success
    	rw.WriteHeader(http.StatusOK)
    } else {
    	//send failure
    	rw.WriteHeader(http.StatusNotFound)    	
    }
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(fetchRet)
}

func fetchHandler(c chan *http.Response, Keys []KeyValue, sID int){
	client := &http.Client{}
	endPoint := servers[sID] + "fetch"
	jsonValue, _ := json.Marshal(Keys)
	request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))	
	if err != nil {
		panic(err)
		return
	}
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
		return
	} else {
		c <- response
	}
}

func fetchAllHandler(c chan *http.Response, sID int){
	client := &http.Client{}
	endPoint := servers[sID] + "fetch"
	request, err := http.NewRequest("GET", endPoint, nil)	
	if err != nil {
		panic(err)
		return
	}
	if DBG != 0 {
		fmt.Println("Sending FetchAll", c)
	}
	response, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
		return
	} else {
		c <- response
	}
}

func fetchAll(rw http.ResponseWriter) {
	serverResp := make(map[int]*http.Response, numServers)
	channels := make([]chan *http.Response, numServers)
	for i := 0; i < numServers; i++ {
		channels[i] = make(chan *http.Response)		
    }
	for sID,_ := range servers {
    	go fetchAllHandler(channels[sID], sID)	
    }
    
    for sID,_ := range servers {
    	select {
			case resp := <- channels[sID]:
				if DBG != 0 {
					fmt.Println("Got Response from Server:", sID)
				}
				serverResp[sID] = resp
			case <-time.After(time.Second * 1):
				fmt.Println("timeout on chan", sID)
    	}
    }
    if DBG != 0 {
	    fmt.Println("Got responses from all Servers")
    }
    sendFetchResponse(rw, serverResp)
}

func fetchReqHandler(rw http.ResponseWriter, req *http.Request) {
	fmt.Println("Serving Fetch Request")
	if req.Method != "GET" && req.Method != "POST" {
		unknownMethodHandler(rw,req)
		return
	}
    if req.Method == "GET" {
    	fetchAll(rw)
    	return
    }
    decoder := json.NewDecoder(req.Body)    
	keys := make([]KeyValue,0)
    err := decoder.Decode(&keys)
    if err != nil {
        panic(err)
        return
    }
    defer req.Body.Close()
	serverKeys := make(map[int][]KeyValue, numServers)
	serverResp := make(map[int]*http.Response, numServers)
	chans := make([]chan *http.Response, numServers)
	for i := 0; i < numServers; i++ {
		chans[i] = make(chan *http.Response)		
    }
    
    for _, Key := range keys {
    	dKey := Key.Data
    	if Key.Encoding == "binary" {
			decoded, err := base64.StdEncoding.DecodeString(Key.Data)
			if err != nil {
				fmt.Println("decode error:", err)
				return
			}
			dKey = string(decoded)
		} 
    	serverID := int(fnvHash(dKey) % uint32(numServers))
    	serverKeys[serverID] = append(serverKeys[serverID], Key)
    }
    
    for sID, keys := range serverKeys {
    	go fetchHandler(chans[sID], keys, sID)	
    }
    
    for sID,_ := range serverKeys {
    	select {
			case resp := <- chans[sID]:
				if DBG != 0 {
					fmt.Println("Got Response from Server:", sID)
				}
				serverResp[sID] = resp
			case <-time.After(time.Second * 1):
				fmt.Println("timeout on chan", sID)
    	}
    }
    sendFetchResponse(rw, serverResp)
}

func sendQueryResponse(rw http.ResponseWriter, sResp map[int]*http.Response){
	//combine and send the output.
	queryRet := make([]QueryReturn, 0)
	isSuccess := true
	for _, response := range sResp {
		defer response.Body.Close()
		body, _ := ioutil.ReadAll(response.Body)
		if DBG != 0 {
			fmt.Println("response Body:", string(body))				
		}
		if response.StatusCode != 200 {
			isSuccess = false			
		}
	    entry := make([]QueryReturn, 0)
	    err := json.Unmarshal(body, &entry)
	    if err != nil {
			log.Fatal(err)
			return
		} 
		queryRet = append(queryRet, entry...)
    }
    if DBG != 0 {
	    fmt.Println("Sending Return To Client", queryRet)
    }
    if isSuccess {
    	//send Success
    	rw.WriteHeader(http.StatusOK)
    } else {
    	//send failure
    	rw.WriteHeader(http.StatusNotFound)    	
    }
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(queryRet)
}

func queryHandler(c chan *http.Response, Keys []KeyValue, sID int){
	client := &http.Client{}
	endPoint := servers[sID] + "query"
	jsonValue, _ := json.Marshal(Keys)
	request, err := http.NewRequest("POST", endPoint, bytes.NewBuffer(jsonValue))
	
	if err != nil {
		panic(err)
		return
	}
	request.Header.Set("Content-Type", "application/json")
	response, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
		return
	} else {
		c <- response
	}
}

func queryAllHandler(c chan *http.Response, sID int){
	client := &http.Client{}
	endPoint := servers[sID] + "query"
	request, err := http.NewRequest("GET", endPoint, nil)	
	if err != nil {
		panic(err)
		return
	}
	response, err := client.Do(request)
	if err != nil {
		log.Fatal(err)
		return
	} else {
		c <- response
	}
}

func queryAll(rw http.ResponseWriter) {
	serverResp := make(map[int]*http.Response, numServers)
	chans := make([]chan *http.Response, numServers)
	for i := 0; i < numServers; i++ {
		chans[i] = make(chan *http.Response)		
    }
	for sID,_ := range servers {
    	go queryAllHandler(chans[sID], sID)	
    }
    for sID,_ := range servers {
    	select {
			case resp := <- chans[sID]:
				if DBG != 0 {
					fmt.Println("Got Response from Server:", sID)
				}
				serverResp[sID] = resp
			case <-time.After(time.Second * 1):
				fmt.Println("timeout on chan", sID)
    	}
    }
    sendQueryResponse(rw, serverResp)
}

func queryReqHandler(rw http.ResponseWriter, req *http.Request) {
	fmt.Println("Serving Query Request")
	if req.Method != "GET" && req.Method != "POST" {
		unknownMethodHandler(rw,req)
		return
	}
	if req.Method == "GET" {
		queryAll(rw)
    	return
    }
    decoder := json.NewDecoder(req.Body)    
	keys := make([]KeyValue,0)
    err := decoder.Decode(&keys)
    if err != nil {
        panic(err)
        return
    }
    defer req.Body.Close()
	serverKeys := make(map[int][]KeyValue, numServers)
	serverResp := make(map[int]*http.Response, numServers)
	chans := make([]chan *http.Response, numServers)
	for i := 0; i < numServers; i++ {
		chans[i] = make(chan *http.Response)		
    }
    
    for _, Key := range keys {
		dKey := Key.Data
    	if Key.Encoding == "binary" {
			decoded, err := base64.StdEncoding.DecodeString(Key.Data)
			if err != nil {
				fmt.Println("decode error:", err)
				return
			}
			dKey = string(decoded)
		}
    	serverID := int(fnvHash(dKey) % uint32(numServers))
    	serverKeys[serverID] = append(serverKeys[serverID], Key)
    }
    
    for sID, keys := range serverKeys {
    	go queryHandler(chans[sID], keys, sID)	
    }
    
    for sID,_ := range serverKeys {
    	select {
			case resp := <- chans[sID]:
				if DBG != 0 {
					fmt.Println("Got Response from Server:", sID)
				}
				serverResp[sID] = resp
			case <-time.After(time.Second * 1):
				fmt.Println("timeout on chan", sID)
    	}
    }
    sendQueryResponse(rw, serverResp)
}

func hello(rw http.ResponseWriter, req *http.Request) {
	switch req.Method{
		case "GET":
			log.Println("GET Request\n")			
			rw.WriteHeader(http.StatusOK)
			rw.Write([]byte("Welcome to Proxy Server"))
		default:
			log.Println("Invalid Request\n")	
			rw.WriteHeader(http.StatusBadRequest)
	}
}

func usage(){
	fmt.Println("Need atleast one server address and Proxy's IPaddr")
	fmt.Println("Usage: ./proxy <IP:Port> <IP:Port> ... <MyIP:Port>")
	os.Exit(-1)
}

func addServers(){
	flag.Parse()
	args := flag.Args()
	if len(args) <= 1 {
		usage()
	}
	n := len(args)
	numServers = n-1
	for i, arg := range args {
		if i == n-1 {
			str := strings.Split(arg, ":")
			if len(str) == 1 {
				myPort = str[0]
			} else {
				myPort = str[len(str) - 1]
			}			
			break;		
		}
		servers[i] = "http://" + arg + "/"
	}
	
}

func main(){
	
	addServers()
	
	http.HandleFunc("/", hello)
	http.HandleFunc("/set", setReqHandler)
	http.HandleFunc("/fetch", fetchReqHandler)
	http.HandleFunc("/query", queryReqHandler)
	
	fmt.Println("Starting Server at:", myPort)
	log.Fatal(http.ListenAndServe(":"+myPort, nil))
}
