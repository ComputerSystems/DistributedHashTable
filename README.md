# Distributed Hash Table 
###### By Bishwajeet Dey and Sriramprabhu Sanakaraguru

### 1. Introduction
A distributed hash table is used to provide a lookup service like a hash
table. However, the key value pairs are stored across several machines since
they cannot fit in a single machine's memory. 

### 2. Architecture
In our architecture, we have a lightweight proxy(proxy/proxy.go) which
distributes keys among servers(server/server.py) which stores the actual key
value pairs. The client writes/reads from the proxy. It does not communicate
directly with the servers. The proxy knows the number of servers and uses a
consistent hashing algorithm to distribute/retrieve keys. 

Following is the architecture diagram:

```
+------+
|  S1  |______________________
|      |                      |  
+------+                      |  
                              |                 
+------+                   +------+             +------+
|  S2  |___________________|  P   |_____________|  C   |
|      |                   |      |             |      | 
+------+                   +------+             +------+
                              |
                              |
+------+                      |  
|  S3  |______________________|  
|      |
+------+
```
S1, S2, S3 are servers which the proxy uses to store and retrieve key value
pairs. Any client C can connect to the proxy P and send/receive requests. The API
is covered under the API section. The client could be curl since it follows the
HTTP specification. The keys are sharded. Currently, there is no replication or
fault tolerance. It will be added in future versions.

### 3. API.
The API follows the specification in the post - https://piazza.com/class/j6zju0uhbwv6fc?cid=108
Examples:

##### 3.1. set request 
curl -v -X PUT --data "@json/set.json"  -H "Content-Type:application/json" http://127.0.0.1:8080/set

where set.json contains the following:
```
[
    {
        "key" : {"encoding": "string", "data": "a"},
        "value": {"encoding": "string", "data": "apple"}
    },
    {
        "key" : {"encoding": "string", "data": "b"},
        "value": {"encoding": "string", "data": "ball"}
    },
    {
        "key" : {"encoding": "string", "data": "c"},
        "value": {"encoding": "string", "data": "cat"}
    }
]
```
So, we added three key value pairs. These keys were distributed by the proxy to
the servers connected to the proxy using consistent hashing.

##### 3.2. fetch request
curl -v -XPOST --data "@json/query.json"  -H "Content-Type:application/json" http://127.0.0.1:8080/fetch

returns
```
[
  {
    "key": {
      "data": "a", 
      "encoding": "string"
    }, 
    "value": {
      "data": "apple", 
      "encoding": "string"
    }
  }, 
  {
    "key": {
      "data": "b", 
      "encoding": "string"
    }, 
    "value": {
      "data": "ball", 
      "encoding": "string"
    }
  }
]
```
with 404 because the last key in the file json/query.json does not exist in the
database. If all keys are found, it returns a 200 OK.

##### 3.3. Query request
curl -v -XPOST --data "@json/query.json"  -H "Content-Type:application/json"\
http://127.0.0.1:8080/query
```
[
  {
    "key": {
      "data": "a", 
      "encoding": "string"
    }, 
    "value": true
  }, 
  {
    "key": {
      "data": "b", 
      "encoding": "string"
    }, 
    "value": true
  }, 
  {
    "key": {
      "data": "noexist", 
      "encoding": "string"
    }, 
    "value": false
  }
]
```
##### 3.4. To check all the key value pairs
curl -v -XGET   http://127.0.0.1:8080/query

returns
```
[
  {
    "key": {
      "data": "b", 
      "encoding": "string"
    }, 
    "value": true
  }, 
  {
    "key": {
      "data": "a", 
      "encoding": "string"
    }, 
    "value": true
  }, 
  {
    "key": {
      "data": "c", 
      "encoding": "string"
    }, 
    "value": true
  }
]
```
### 4. Installation

##### 4.1 proxy
    The proxy is in golang. To install it in Ubuntu use,
     sudo apt-get install golang-go

##### 4.2 server
    Server needs python3. It uses Flask. Please execute the following instructions
    sudo pip3 install virtualenv python3-venv
    pip3 install --upgrade pip setuptools
    cd DHT/server
    python3 -m venv venvs/DHT
    source venvs/DHT/bin/activate #This activates virtual env and you should see DHT prefix in shell
    pip3 install flask requests

### 5. Running the installation
   ##### 5.1 Proxy
   The proxy reads the server list from a file server_list.txt. The firt line
    contains the number of servers - N. N servers follow. 
   
   The proxy can be started with:
   make - To make the proxy binary
   ```
   ./Proxy <IPs1:Port1> <IPs2:Port2> ... <ProxyIP: Port>
   ```
   By default it starts at 8080. An additional port parameter can also be given
    to start the proxy at a different port
    
   ##### 5.2 Server
    The server can be started within venv environment:
    ```
    python3 server.py <optional-port>
    ```
    By default, the server starts at port 8080. It can be changed by adding an
    optional port parameter.


### OTHER NOTES:
1. Example json files are located in json/ directory
        



