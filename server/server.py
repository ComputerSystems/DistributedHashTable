#!/usr/bin/env python3
from flask import Flask, request, Response, jsonify

import base64
import sys
import threading

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False

db = {}

lock = threading.Lock()

@app.route("/")
def index():
    print(request.host.split(':')[0])
    return Response("DHT Server running at " + request.host), 200

def decode(val_tuple):
    if not('encoding' in val_tuple):
        raise ValueError("No encoding found")

    if not('data' in val_tuple):
        raise ValueError("No data found")

    val_type, val = val_tuple['encoding'], val_tuple['data']
    if (val_type == 'binary'):
        val = base64.b64decode(val).decode()
    elif (val_type == 'string'):
        pass
    else:
        raise ValueError("Malformed key")

    return val

def check_or_raise_key(key_value):
    if not('key' in key_value):
        raise ValueError("No key found")

def check_or_raise(key_value):
    check_or_raise_key(key_value)

    if not('value' in key_value):
        raise ValueError("No value found")


def actual_key_value(key_value):
    check_or_raise(key_value)

    return decode(key_value['key']), decode(key_value['value'])

def pack(value):
    return {"encoding" : "string", "data" : value}

@app.route("/set", methods = ['PUT'])
def set():
    try:
        json = request.get_json()
        errors = []
        ok = 0
        for key_value in json:
            try:
                check_or_raise(key_value)
                key, value = actual_key_value(key_value)
                with lock:
                    db[key] = value
                ok += 1
            except Exception as e: #Value Error or type different from expected
                errors.append(key_value)

    except Exception: #Top level exception. Cannot iterate
        return jsonify({"keys_added" : 0, "keys_failed" : request.data}), 404

    response = {"keys_added" : ok, "keys_failed" : errors}

    return jsonify(response), 200 if len(errors) == 0 else 404

@app.route("/fetch", methods = ['POST', 'GET'])
def fetch():
    results = []
    errors = False

    try:
        json = request.get_json()
        if (request.method == 'GET' or json == None):
            for key, value in db.items():
                results.append({"key" : pack(key), "value" : pack(value)})
        else:
            for key_tuple in json:
                try:
                    key = decode(key_tuple)
                    with lock:
                        if (key in db):
                            results.append({"key" : pack(key), "value" : pack(db[key])})
                        else:
                            errors = True
                except Exception as e: #Value Error or type different from expected
                    errors = True

    except Exception: #Top level exception. Cannot iterate
        return jsonify([]), 404

    return jsonify(results), 200 if not(errors) else 404

@app.route("/query", methods = ['POST', 'GET'])
def query():
    results = []
    errors = False

    try:
        json = request.get_json()
        if (request.method == 'GET' or json == None):
            for key, value in db.items():
                results.append({"key" : pack(key), "value" : True})
        else:
            for key_tuple in json:
                try:
                    key = decode(key_tuple)
                    with lock:
                        exists = key in db

                    results.append({"key" : pack(key), "value" : exists})
                    if not(exists):
                        errors = True
                except Exception as e: #Value Error or type different from expected
                    errors = True

    except Exception: #Top level exception. Cannot iterate
        return jsonify([]), 404

    return jsonify(results), 200 if not(errors) else 404


if __name__ == "__main__":
    port = int(sys.argv[1]) if len(sys.argv) == 2 else 8080
    print("Starting server on {}".format(port))
    app.run(debug=True, port = port)
