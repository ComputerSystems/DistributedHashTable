#!/usr/bin/env python3

import requests
import json
import base64

set_payload_items = [
    {
        "key":  {"encoding": "string", "data": "First Key"},
        "value":{"encoding": "string", "data": "First Value"}
    },
    {
        "key":  {"encoding": "string", "data": "Second Key"},
        "value":{"encoding": "string", "data": "Second Value"}
    },
    {
        "key":  {"encoding": "binary", "data": base64.b64encode('Binary Key'.encode()).decode()},
        "value":{"encoding": "binary", "data": base64.b64encode('Binary Value'.encode()).decode()}
    }
    ]

fetch_payload_items = [
        {"encoding": "string", "data": "First Key"},
        {"encoding": "string", "data": "Your house Key"}
]
#Test SET endpoint 
set_response = requests.put("http://127.0.0.1:8081/set", json = set_payload_items)

fetch_response = requests.post('http://127.0.0.1:8081/fetch', json= fetch_payload_items)

get_fetch_response = requests.get('http://127.0.0.1:8081/fetch')

query_response = requests.post('http://127.0.0.1:8081/query', json= fetch_payload_items)

get_query_response = requests.post('http://127.0.0.1:8081/query')

print(set_response, set_response.content)

print(fetch_response, fetch_response.content)

print(query_response.content.decode('UTF-8'))

print("GET FETCH:" + get_fetch_response.content.decode('UTF-8'))

print("GET QUERY:" + get_query_response.content.decode('UTF-8'))



