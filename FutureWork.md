# Consistent Hashing, Load-balancing and Fault tolerance
*(Extension to HW6 submitted by Sriramprabhu Sankaraguru & Bishwajeet Dey)*
### 1. Introduction
We have provided Implementation for Distributed Hash Table that can get/set key-value pairs in a fixed number of servers. 
The implementation computes hash of the key and takes mod n on the hash to find the appropriate node on which the data
must be stored. The problem is that the number of servers can't grow or shrink without substantial key-value pair 
exchanges between all nodes in the setup. In this paper, we are providing solutions to tackle the problem using consistent
hashing. We also talk about how to handle load-balancing and fault tolerance.
### 2. Requirements
The number of servers in the setup should be able to grow/shrink with minimal amount of key-value pair transfers between
the servers. When a server is added, there must be very minimal key-value pair exchanges. When a node is deleted, the keys
in that server should be moved to remapped without affecting the other servers. The servers in the environment
may be having different capabilities. Some servers can handle large key-value pair where as other servers may have limited 
resources and couldn't handle much of the data. We must provide a way for balancing the load between the servers. Also, when
a server or proxy goes down, we must be able to handle it and continue to provide the service.
### 3. Proposed Design
##### 3.1 Scalability
The number of servers in the setup must be scalable. When a new server is added or an old server is removed, we must remap keys 
so that key look ups don't fail/fetch wrong data. To reduce the remapping of data between servers while adding/deleting a server, we are going to use consistent hashing. We define a ring of fixed size and map the servers and key-value pairs to that point on the ring. The ring size 
is fixed at 2^32. This means that there can at most 2^32 servers can be added. Now, each server gets all the keys that have
hash value greater than its own hash value and less than next hash value. If we visualize the ring as the clock and servers
to be mapped at each 5-minute mark, then each server gets all the minutes that are clock-wise next to the current mark and previous
to the next mark. (5 gets key-value pairs that are mapped to 5,6,7,8,9) We use CRC32 sum for getting the hash value of the
servers and the keys. Each Server will get a unique label and CRC32 sum will be calculated on that label. For data,
CRC32 will be computed on the key. We will see how it works when we add a server and remove an existing server. 
###### 3.1.1 Adding a server
When adding a server, a new unique label is assigned to that server. CRC32 sum is computed on that label. It is added to 
the ring. Now this server can start accepting data. We must remap the existing keys from the neighbor so that further 
key look ups don't fail. To achieve this, all we should do is, get all the keys from its previous node. Each of the keys should be 
checked and if necessary, remapped to the new server. This doesn't affect the existing setup at all. This new server
can start accepting key-value pairs as usual. 
###### 3.1.2 Removing a server
Similarly, when a server is removed, the setup is not at all affected. We don't have to do anything other than remapping the 
keys in the current server to the adjacent servers. The process is similar to adding a server. The particular node is removed from
the ring maintained by the proxy.

As you can see, the average number of remapping required in case of addition/removal is K/n. Where K is the number of Keys and n is
number of servers. 


###### 3.1.3 Put key-value pair
When a put request comes to the proxy, it computes the CRC32 sum for the key. This hash is then used to find the appropriate 
node on which this data can be placed. The proxy does a look up on the ring and compares the hash of the server node. We find the node on which the key's hash must fall under. A node is selected, When the node's hash is greater than the key's hash and the key's hash is less than next node's hash. Last node get the key if the key's hash is greater than it's hash value. The put request is redirected to that server and the data is placed there.
###### 3.1.4 Get key-value pair
This process is similar to put request. Proxy finds the node to which the request has to be forwarded by searching the ring. Now the get request is forwarded to that particular server.

Implementation for this can be provided as the following:
- Ring is maintained as a list of nodes sorted by the hash value. 
- Each node contains the label, hash value and end point to reach the server (Ex. http://192.168.0.121:8080/).
- When a key-value pair must be mapped, we compute hash of the key and do binary search in the list to find the appropriate node on which the key can be mapped.
- The same is done for get request. If the server doesn't contain the key, it returns 404 error.
- When adding a server, we assign a label, compute hash and find the appropriate location where the node can be added in the sorted list
- We check all the keys from the previous node and remap them to this new server if needed. (i.e. If the key's hash > node's hash)
- Similarly, for removal, the appropriate node is found and removed from the ring.
- Its keys are remapped to its previous node.
- For put request, the key's hash is computed and we do binary search in the list to find the node on which the data should reside. (where the key's hash > node's hash and key's hash < next node's hash). 
- From the node, we can get the end point and forward the request to that end point.
- Similarly we handle get request.

##### 3.2 Load Balancing
Each server in the environment will have different set of capabilities. Some servers may be able to store double the number of keys 
than the other server. So, we need to utilize those servers and map comparatively more keys than the rest of the servers. To do this,
we duplicate the node of that server multiple times in the ring. Say a server C has capability to store more data, we assign a weight 
to that server ex. 10. This server will be replicated as if there are 10 C nodes in the ring. We assign labels C[0-9] and compute hash 
for each label. And we place them all in the ring. Each node will have different label, hash value but they all point to the same end point so the put/get request is forwarded to the same server. Thus the probability
of this node getting a key is increased by adding duplicate nodes in the ring. When we remove this node, we have to remove all 
the duplicate nodes in the ring.

##### 3.3 Fault-Tolerance
We could solve this problem by replicating the servers. We can maintain 2 replicas for each server. Every put request the original server gets, will be forwarded to those replica servers. Proxy should maintain a table of replicas of each server. In case of failure, the proxy can look up the table and find the replica server. It then, redirects the request to that server. Detecting server failure is straight forward if the server's endpoint becomes unreachable. In addition to that, The proxy server will periodically try to ping each server by walking through the nodes in the ring. If it finds failure, it can start using the replica. Also, Each server writes the data to a json file instead of just keeping the data in memory. After each put request, they write the whole data to the file. For get request, they can return the value from the in-memory dictionary object. This can be used while restarting server in case of failure. Server program, during start up, always reads the json file and populates the in-memory dictionary object. Json file name can be hard coded as the filename is standard and pre-agreed. If the file is not found, the server program creates a fresh file and saves the data as usual.

The same idea can be used in proxy side. Proxy can maintain a replica which just stores the ring data structure and the server- replica table. The original proxy server just sends update requests whenever a new server is added or old server is removed. In case of failure, the client can access the replica server. Since the replica server has the ring and replica table, it can handle the client's requests hereafter. 

### 4. Conclusion
Thus, we can solve the scalability problem using consistent hashing and the load balancing through duplicating the nodes in the ring. We can achieve fault-tolerance by replicating the servers.
